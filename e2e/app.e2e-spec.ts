import { MyAppFrontendPage } from './app.po';

describe('my-app-frontend App', () => {
  let page: MyAppFrontendPage;

  beforeEach(() => {
    page = new MyAppFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

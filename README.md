# Frontend for MyProject
Source code of demo application [MyProject](https://bitbucket.org/TRAHOMOTO/my-project-backend)
Demonstrate authentication, dynamic routing and module-based approach to develop large single page applications.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0.

## Todo

1. **Tests**
2. JWT Authentication
3. i18n
4. Tree Shaking

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. Dev server has preconfigured proxy 
for make api calls easier (see **proxy.conf.json** in projects root folder).
The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. 
Use the `ng build --prod --aot=true` for a production build with Ahead Of Time compilation.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

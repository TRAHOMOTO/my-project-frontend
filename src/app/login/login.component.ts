import {Component, HostBinding, OnInit} from '@angular/core';
import {fadeInAnimation} from '../animations';
import {UserService} from '../_services/user.service';
import {Router} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RouterConfigService} from '../_services/router-config.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations: [fadeInAnimation]
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  error: any;

  @HostBinding('@fadeInAnimation') fadeInAnimation = true;
  @HostBinding('style.display') display = 'block';
  constructor(public user: UserService,
              private router: Router,
              private title: Title,
              private fb: FormBuilder,
              private routerConf: RouterConfigService) {
  }

  ngOnInit() {
    this.buildForm();
    this.authorizeCurrentUser();
  }

  buildForm() {
    this.loginForm = this.fb.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  authorizeCurrentUser() {
    this.error = null;
    this.user.loadCurrentUser()
      .then(this.authorisation.bind(this))
      .catch( err => {
        this.error = err;
      });
  }

  authorisation() {
    if (this.user.isLogIn()) {
      this.prepareRoutingForUser();
    } else {
      this.title.setTitle('Login please');
    }
  }

  authentication(login: string, password: string): void {
    this.error = null;
    this.user.logIn({login, password})
      .then(this.prepareRoutingForUser.bind(this))
      .catch( err => {
        this.error = err;
      });
  }

  prepareRoutingForUser() {
    this.routerConf.rebuildRouter(this.user.currentUser);
    this.router.navigate(['admin/']);
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.authentication(this.loginForm.value.login, this.loginForm.value.password);
    }
  }

  isLoginValid() {
    return !(this.loginForm.controls.login.invalid && this.loginForm.controls.login.dirty);
  }
  isPasswordValid() {
    return !(this.loginForm.controls.password.invalid && this.loginForm.controls.password.dirty);
  }
  isFormInvalid() {
    return this.loginForm.invalid;
  }
}

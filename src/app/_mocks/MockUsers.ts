import {User} from '../_beans/user';

export const MOCK_ADMIN = new User({uid: 1, name: 'mock_admin', roles: ['admin'] } as any);
export const MOCK_USER = new User({uid: 2, name: 'mock_user', roles: ['user'] } as any);
export const MOCK_ADMIN_USER = new User({uid: 1, name: 'mock_admin_user', roles: ['admin', 'user'] } as any);
export const MOCK_USER_ADMIN = new User({uid: 2, name: 'mock_user_admin', roles: ['user', 'admin'] } as any);
export const MOCK_ANONYMOUS = new User({uid: 0, name: 'anonimous', roles: [] } as any);

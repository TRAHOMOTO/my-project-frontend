import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserDashboardComponent} from './user-dashboard/user-dashboard.component';
import {UserHomeComponent} from "./user-home/user-home.component";
import {AuthGuard} from "../auth.guard";

const userRoutes: Routes = [
  {
    path: 'admin',
    component: UserHomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        children: [
          {path: 'dashboard', component: UserDashboardComponent},
          {path: '', redirectTo: 'dashboard', pathMatch: 'full'}
        ],
      }
    ]
  }
];



@NgModule({
  imports: [
    RouterModule.forChild(userRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class UserRoutingModule { }

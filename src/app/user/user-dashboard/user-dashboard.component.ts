import {Component, HostBinding, OnInit} from '@angular/core';
import {UserService} from '../../_services/user.service';
import {Title} from '@angular/platform-browser';
import {User} from '../../_beans';
import {fadeInAnimation} from '../../animations';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css'],
  animations: [fadeInAnimation]
})
export class UserDashboardComponent implements OnInit {
  @HostBinding('@fadeInAnimation') fadeInAnimation = true;
  @HostBinding('style.display') display = 'block';
  constructor(private _user: UserService,
              private title: Title) { }

  ngOnInit() {
    this.title.setTitle('User dashboard');
  }
  get user(): User{
    return this._user.currentUser;
  }

}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserDashboardComponent} from './user-dashboard/user-dashboard.component';
import {UserRoutingModule} from './user-routing.module';
import {UserHomeComponent} from './user-home/user-home.component';

@NgModule({
    declarations: [
        UserDashboardComponent,
        UserHomeComponent
    ],
    imports: [
        CommonModule,
        UserRoutingModule
    ],
    exports: []
})
export class UserModule {
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {Page404Component} from './page404/page404.component';
import {LogoutComponent} from './logout/logout.component';


const appRoutes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'logout', component: LogoutComponent, outlet: 'modal'},

    {path: '', redirectTo: '/login', pathMatch: 'full'},
    {path: '**', component: Page404Component}
];


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forRoot(appRoutes, {useHash: true})
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {
}

export class User {
  uid: number;
  login: string;
  roles: string[];

  constructor({uid, login, roles = [] }) {
    this.uid = uid;
    this.login = login;
    this.roles = roles;
  }

  hasRole(roleName: string): boolean {
    return this.roles.indexOf(roleName) !== -1;
  }
}

import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../_services/user.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private router: Router,
              private user: UserService) { }

  ngOnInit() {
  }

  logout() {
    this.user.logOut().then(res => {
      if (res.success) this.gotoRoot();
    });
  }

  gotoRoot() {
    this.router.navigate(['/']);
  }

  cancel() {
    this.closeModal();
  }
  closeModal() {
    this.router.navigate([{ outlets: { modal: null }}]);
  }

}

import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {Page404Component} from './page404/page404.component';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {LoginComponent} from './login/login.component';
import {UserService} from './_services/user.service';
import {AppRoutingModule} from './app-routing.module';
import {AdminModule} from './admin/admin.module';
import {UserModule} from './user/user.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LogoutComponent} from './logout/logout.component';
import {AuthGuard} from './auth.guard';
import {RouterConfigService} from "./_services/router-config.service";


const CoreAngular = [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule
];

@NgModule({
    imports: [
        ... CoreAngular,

        AdminModule,
        UserModule,

        AppRoutingModule,
    ],
    declarations: [
        AppComponent,
        Page404Component,
        LoginComponent,
        LogoutComponent
    ],
    providers: [
        RouterConfigService,
        UserService,
        AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

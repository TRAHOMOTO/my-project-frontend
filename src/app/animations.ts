import { trigger, state, style, transition, animate } from '@angular/animations';

// Component transition animations
export const fadeInAnimation =
    trigger('fadeInAnimation', [
        state('*',
            style({
                opacity: 1
            })
        ),
        transition(':enter', [
            style({
                opacity: 0
            }),
            animate('0.2s ease-in')
        ])
    ]);

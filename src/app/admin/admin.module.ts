import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminDashboardComponent} from './admin-dashboard/admin-dashboard.component';
import {AdminHomeComponent} from './admin-home/admin-home.component';

@NgModule({
    declarations: [
        AdminDashboardComponent,
        AdminHomeComponent
    ],
    imports: [
        CommonModule,
        AdminRoutingModule
    ],
    exports: [
        AdminDashboardComponent
    ]
})
export class AdminModule {
}

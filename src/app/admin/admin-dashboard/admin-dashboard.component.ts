import {Component, HostBinding, OnInit} from '@angular/core';
import {fadeInAnimation} from 'app/animations';
import {Title} from '@angular/platform-browser';
import {User} from '../../_beans';
import {UserService} from '../../_services/user.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  animations: [fadeInAnimation]
})
export class AdminDashboardComponent implements OnInit {

  @HostBinding('@fadeInAnimation') fadeInAnimation = true;
  @HostBinding('style.display') display = 'block';
  constructor(private title: Title,
              private _user: UserService) {
  }

  ngOnInit() {
    this.title.setTitle('Admin dashboard');
  }

  get user(): User{
    return this._user.currentUser;
  }

}

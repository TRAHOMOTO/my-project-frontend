import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import {User, UserCredentials} from '../_beans';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/delay';
import {environment} from '../../environments/environment.prod';

export interface UserServiceResult { success: boolean, message: string
}

@Injectable()
export class UserService {

  _currentUser: User = null;

  rootApiUrl = environment.apiUrl.root;

  constructor(private http: Http) {
  }

  get currentUser() {
    return this._currentUser ? this._currentUser : new User({uid: 0, login: ''});
  }

  isLogIn() {
    return !!this.currentUser.uid;
  }

  loadCurrentUser() {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers});
    return this.http.get(`${this.rootApiUrl}/user`, options)
      .map(resp => resp.json())
      .do(user => this._currentUser = new User(user))
      .toPromise();
  };

  logIn(creds: UserCredentials): Promise<User | null> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers});

    return this.http
      .post(`${this.rootApiUrl}/login`, creds, options)
      .map(resp => resp.json())
      .do(user => this._currentUser = new User(user))
      .toPromise();
  }

  logOut(): Promise<UserServiceResult> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers});

    return this.http
      .get(`${this.rootApiUrl}/logout`, options)
      .map(resp => resp.json())
      .do(res => {
        if (res.success) {
          this._currentUser = null;
        }
      })
      .toPromise();
  }
}

import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../_beans/user';
import {UserHomeComponent} from '../user/user-home/user-home.component';
import {AdminHomeComponent} from '../admin/admin-home/admin-home.component';

@Injectable()
export class RouterConfigService {

  _originalRouterConfig: any[];

  constructor(private router: Router) {
    this._originalRouterConfig = this.router.config;
  }

  get originalRouterConfig(){
    return this._originalRouterConfig;
  }

  rebuildRouter(user: User): void {
    if (user.hasRole('admin')) {
      const newRoute = this.computeUpdatedRoutesWithout('admin', UserHomeComponent);
      this.router.resetConfig(newRoute);
      return;
    } else if (user.hasRole('user')) {
      const newRoute = this.computeUpdatedRoutesWithout('admin', AdminHomeComponent);
      this.router.resetConfig(newRoute);
      return;
    }
  }

  computeUpdatedRoutesWithout(path: string, component: any): any[] {
    return this.originalRouterConfig.reduce((acc, route) => {
      if (route.path === path && route.component === component) {
        return acc;
      }
      acc.push(route);
      return acc;
    }, []);
  }
}
